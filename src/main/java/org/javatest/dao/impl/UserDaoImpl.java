package org.javatest.dao.impl;

import org.javatest.dao.AbstractDao;
import org.javatest.dao.UserDao;
import org.javatest.dao.model.TestUser;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;
import java.util.Queue;

@Repository("userDao")
public class UserDaoImpl  extends AbstractDao<TestUser> implements UserDao {
    @Override
    public List<TestUser> getLastLoggedInUsers() throws Exception {
        String query="";
        return getEntityManager().createQuery(query).getResultList();
    }

    @Override
    public TestUser findByUserName(String userName) throws Exception {
        Query query=getEntityManager().createQuery("SELECT tu FROM TestUser tu WHERE username=:uname");
        query.setParameter("uname",userName);
        List result=query.getResultList();
        return result.isEmpty()?null:(TestUser) result.get(0);
    }
}
