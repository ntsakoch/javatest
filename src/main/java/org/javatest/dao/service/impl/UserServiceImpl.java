package org.javatest.dao.service.impl;

import org.javatest.dao.UserDao;
import org.javatest.dao.model.TestUser;
import org.javatest.dao.service.UserService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public void addUser(TestUser testUser) throws Exception{
      userDao.create(testUser);
    }

    @Override
    public void updateLastLoginTime(TestUser testUser) throws Exception {
        userDao.update(testUser);
    }

    public  List<TestUser> findAllUsers() throws Exception
    {
       return userDao.findAll();
    }

    @Override
    public List<TestUser> getLastLoggedInUsers() throws Exception {
        return userDao.getLastLoggedInUsers();
    }

    @Override
    public TestUser getUserByUsername(String username) throws Exception {
        return userDao.findByUserName(username);
    }
}
