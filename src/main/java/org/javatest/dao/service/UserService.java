package org.javatest.dao.service;

import org.javatest.dao.model.TestUser;

import java.util.List;
public interface UserService {
    void addUser(TestUser testUser) throws Exception;
    void updateLastLoginTime(TestUser testUser) throws Exception;
    List<TestUser> findAllUsers() throws Exception;
    List<TestUser> getLastLoggedInUsers() throws Exception;
    TestUser getUserByUsername(String username) throws  Exception;
}
