package org.javatest.dao;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ntsako on 2017/03/28.
 */
public abstract class AbstractDao <E> {
    private Class<?> entityClass;

    public AbstractDao() {
        this.entityClass = (Class<?>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @PersistenceContext
    private EntityManager entityManager;

    public void create(E entity) throws Exception{
        entityManager.persist(entity);
        System.out.println("Entity persisted: "+entity);
    }

    public void update(E entity) throws  Exception
    {
        entityManager.merge(entity);
        System.out.println("Entity merged: "+entity);
    }
    public List<E> findAll() throws Exception
    {
        Query query = entityManager.createQuery("SELECT e from "+entityClass.getSimpleName()+" e");
        return (List<E>) query.getResultList();
    }

    protected EntityManager getEntityManager()
    {
        return entityManager;
    }
}
