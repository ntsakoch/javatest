package org.javatest.dao;

import org.javatest.dao.model.TestUser;

import java.util.List;

public interface UserDao extends GenericDAO<TestUser>{
    List<TestUser> getLastLoggedInUsers() throws Exception;
    TestUser findByUserName(String userName) throws  Exception;
}
