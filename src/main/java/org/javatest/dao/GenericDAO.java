package org.javatest.dao;

import java.util.List;

/**
 * Created by Ntsako on 2017/03/28.
 */
public interface GenericDAO<E> {

    void create(E entity) throws Exception;

    void update(E entity) throws  Exception;

    List<E> findAll() throws Exception;


}
