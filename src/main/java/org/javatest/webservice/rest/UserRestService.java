package org.javatest.webservice.rest;

import org.apache.catalina.User;
import org.javatest.dao.model.TestUser;
import org.javatest.dao.service.UserService;

import org.javatest.spring.security.SecurityService;
import org.javatest.web.HttpSessionTracker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@RestController
@RequestMapping(path="/api")
@EnableAutoConfiguration
public class UserRestService {

    @Autowired
    private SecurityService securityService;

    @Autowired
    private UserService userService;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @RequestMapping(value = "/users", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, String> getAllRegisteredUsers()
    {
        Map<String,String> result=new HashMap<String, String>();
        try {
            for(TestUser user:userService.findAllUsers())
            {
                result.put(String.valueOf(user.getId()),user.getPhone());
            }
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
        }
        return result;
    }

    @RequestMapping(value = "/users/recent", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,String> getLastLoginUsers()
    {
       Map<String,String> result=new HashMap<String, String>();

        return result;
    }
    @RequestMapping(value = "/user/add",method =RequestMethod.PUT,
            produces =MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,String> addUser(@RequestBody TestUser user) throws Exception {
        Map<String,String> result=new HashMap<String, String>();
        try {
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userService.addUser(user);
            result.put("status","success");
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            result.put("status","error");
        }
        return result;
    }

    @RequestMapping(value = "/user/login",method =RequestMethod.POST
            ,produces =MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,String> userLogin(@RequestBody TestUser user)
    {
      Map<String,String> result=new HashMap<String, String>();
        try {
            System.out.println("on userLogin: "+user);
            securityService.autologin(user.getUsername(),user.getPassword());
            TestUser loggedInUser=userService.getUserByUsername(user.getUsername());
            result.put("id",String.valueOf(loggedInUser.getId()));
            result.put("token", HttpSessionTracker.getLoggedInUserSessionId());
            loggedInUser.setLastLoginTime(new Date());
            userService.updateLastLoginTime(loggedInUser);
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
            result.put("status","error");
        }
        System.out.println("returning result after login: "+result);
        return result;
    }

}
