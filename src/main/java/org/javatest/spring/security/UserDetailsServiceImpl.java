package org.javatest.spring.security;


import org.javatest.dao.UserDao;
import org.javatest.dao.model.TestUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService{

    @Autowired
    private UserDao userDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        TestUser user = null;
        try {
            user = userDao.findByUserName(username);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(user==null)
        {
            throw  new UsernameNotFoundException("User Not Found.");
        }
        Set<GrantedAuthority> grantedAuthorities = new HashSet<GrantedAuthority>();
       grantedAuthorities.add(new SimpleGrantedAuthority("Admin"));
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }
}
