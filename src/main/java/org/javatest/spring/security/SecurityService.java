package org.javatest.spring.security;

public interface SecurityService {
    String findLoggedInUsername();
    void autologin(String username, String password);
}
