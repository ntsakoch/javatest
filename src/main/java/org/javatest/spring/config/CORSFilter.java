package org.javatest.spring.config;

import org.javatest.web.HttpSessionTracker;
import org.springframework.context.annotation.Configuration;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CORSFilter implements Filter {

        @Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) res;
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "180");
		response.setHeader("Access-Control-Allow-Headers", "X-requested-with, Content-Type");
		HttpSessionTracker.setLoggedInUserSessionId(((HttpServletRequest)req).getSession().getId());
		chain.doFilter(req, res);
	}


	public void init(FilterConfig filterConfig) {

	}

	public void destroy() {}

}