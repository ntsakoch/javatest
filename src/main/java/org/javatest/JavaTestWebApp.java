package org.javatest;

import org.javatest.web.HttpSessionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class JavaTestWebApp extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(JavaTestWebApp .class);
    }

   @Bean
    public HttpSessionHandler httpSessionHandler() {
        return new HttpSessionHandler();
    }
    public static void main(String[] args) {
        SpringApplication.run(JavaTestWebApp.class, args);
        //System.out.println("Web App is running...'");
    }
}
