package org.javatest.web;

/**
 * Created by Ntsako on 2017/03/28.
 */
public class HttpSessionTracker {
    private  static ThreadLocal<String> activeSessions=new ThreadLocal<String>();

    public static String getLoggedInUserSessionId()
    {
        try {
            return activeSessions.get();
        }
        catch (NullPointerException exc)
        {
            return "";
        }
    }

    public static void  setLoggedInUserSessionId(String sessionId)
    {
        activeSessions.set(sessionId);
    }

    public static void  removeFromLoggedInUserSessionId()
    {
        activeSessions.remove();
    }
}
