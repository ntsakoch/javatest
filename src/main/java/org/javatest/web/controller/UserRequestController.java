package org.javatest.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Ntsako on 2017/03/28.
 */
@Controller
public class UserRequestController {

    @RequestMapping("/")
    public String getIndexPage(Model model)
    {
      return "index";
    }

    @RequestMapping("/register")
    public String getRegisterPage(Model model)
    {
        return "register";
    }
}
