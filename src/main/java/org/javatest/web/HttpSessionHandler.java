package org.javatest.web;


import org.springframework.security.web.session.HttpSessionEventPublisher;

import javax.servlet.http.HttpSessionEvent;

/**
 * Created by Ntsako on 2017/03/28.
 */
public class HttpSessionHandler extends HttpSessionEventPublisher {

    public void sessionCreated(HttpSessionEvent event) {
      super.sessionCreated(event);
      event.getSession().setMaxInactiveInterval(3*60);
        System.out.printf("session created: "+event);
    }

    public void sessionDestroyed(HttpSessionEvent event) {
        System.out.println("Session Destroyed: "+event);
    }
}
