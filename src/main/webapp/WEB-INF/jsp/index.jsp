<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head lang="en">
    <title>Java Test</title>
</head>
<body>
<h1>Login Page</h1>
<p id="response" > Hello World!</p>
<form id="login-form">
    <table align="center" width="50%">
        <tr>
            <td>Username: </td>
            <td><input type="text" id="username"></td>
        </tr>
        <tr>
            <td>Password: </td>
            <td><input type="password" id="password"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Login" onclick="submitLogin()"></td>
        </tr>
        <tr>
            <td></td>
            <td><a href="<c:url value="/register"/>">Register</a></td>
        </tr>
    </table>

</form>
</body>
<script src="<c:url value="/static/scripts/jquery-1.6.4.min.js"/>" type="text/javascript"></script>
<script type="text/javascript">
    function submitLogin() {
        var user = {};
        user["username"] = $("#username").val();
        user["password"]=$("#password").val();

        $.ajax(
            {
                type: "POST",
                contentType: "application/json",
                url: "/api/user/login",
                data: JSON.stringify(user),
                dataType: 'json',
                success: function (data) {
                    console.log("SUCCESS", data);
                    var json="<p>Registration Response:"+JSON.stringify(data)+" </p>"
                    $("#response").html(json);
                },
                error : function (e) {
                    console.log("Error: ",e);
                },
                done : function (e) {
                    console.log("Done",e);
                }
            }
        );
    }
</script>
</html>