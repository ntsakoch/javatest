<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head lang="en">
    <title>Java Test</title>
</head>
<body>
<h1>Register Page</h1>
<div id="response" ></div>
<form id="register-form">
    <table align="center" width="50%">
        <tr>
            <td>Username: </td>
            <td><input type="text"  id="username"></td>
        </tr>
        <tr>
            <td>Phone: </td>
            <td><input type="text"  id="phone"></td>
        </tr>
        <tr>
            <td>Password: </td>
            <td><input type="password"  id="password"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Submit" onclick="submitRegistration()"></td>
        </tr>
        <tr>
            <td></td>
            <td><a href="<c:url value="/"/>">Login</a></td>
        </tr>
    </table>

</form>
<script src="<c:url value="/static/scripts/jquery-1.6.4.min.js"/>" type="text/javascript"></script>
<script type="text/javascript">

   function submitRegistration() {
       var user = {};
       user["username"] = $("#username").val();
       user["phone"]=$("#phone").val();
       user["password"]=$("#password").val();

       $.ajax(
           {
               type: "PUT",
               contentType: "application/json",
               url: "/api/user/add",
               data: JSON.stringify(user),
               dataType: 'json',
               success: function (data) {
                   console.log("SUCCESS", data);
                   var json="<p>Registration Response:"+JSON.stringify(data)+" </p>";
                   $("#response").html(json);
               },
               error : function (e) {
                   console.log("Error: ",e);
               },
               done : function (e) {
                   console.log("Done",e);
               }
           }
       );
   }
</script>

</body>
</html>